import * as crypto from 'crypto';
import { deterministicPartitionKey } from "./dpk";

describe("deterministicPartitionKey", () => {

  it("Returns the literal '0' when given no input", () => {
    const trivialKey = deterministicPartitionKey();
    expect(trivialKey).toBe("0");
  });
  

  it("Returns the hashed '{}' when given an empty object", () => {

    const trivialKey = deterministicPartitionKey({});

    let expectedKey = JSON.stringify({});
    expectedKey = crypto.createHash("sha3-512").update(expectedKey).digest("hex");
    
    expect(trivialKey).toBe(expectedKey);

  });


  it("Returns 'partitionKey' when given an object containing 'partitionKey'", () => {

    const partitionKey = "partitionKey";
    const trivialKey = deterministicPartitionKey({ partitionKey });
    
    expect(trivialKey).toBe(partitionKey);
    
  });


  it("Returns a 256 character hash when given an object containing a partitionKey larger than 256", () => {

    let partitionKey = new Array(257);
    partitionKey.fill('0');

    partitionKey = partitionKey.join('');
    const expectedKey = crypto.createHash("sha3-512").update(partitionKey).digest("hex");

    const trivialKey = deterministicPartitionKey({ partitionKey });
    
    expect(trivialKey).toBe(expectedKey);

  });


  it("Returns hash of event when given an object containing an empty string", () => {

    const partitionKey = "";
    const trivialKey = deterministicPartitionKey({ partitionKey });
    
    const expectedKey = crypto.createHash("sha3-512").update(
      JSON.stringify({ partitionKey })
    ).digest("hex");

    expect(trivialKey).toBe(expectedKey);
    
  });


  it("Returns the hashed '{}' when given an empty object is passed into paritionKey", () => {

    const partitionKey = {};
    const trivialKey = deterministicPartitionKey({ partitionKey });

    let expectedKey = JSON.stringify(partitionKey);
    
    expect(trivialKey).toBe(expectedKey);

  });

});

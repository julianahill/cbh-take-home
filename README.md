# Introduction
This project contains two challenges - a "Ticket Breakdown" challenge and a "Refactoring" challenge. The two challenges are not related to one another, but both should be completed in the same folder. [You can access a starter folder here](https://drive.google.com/file/d/1xRs7QcJXMfHcPpkvR3rfQB5bAh-EercB/view?usp=sharing). Any written answers should be included in markdown files within that folder.

## Instructions
1. [Ticket Breakdown](./Instructions/Ticket_Breakdown.md)
2. [Refactoring](./Instructions/Refactoring.md)

If you are a JS novice, here's how to get started:
1. [Install Node.js](https://nodejs.org/en/download/) (we use `^16`, the latest LTS)
2. Run `npm i` in this repo to install dependencies
3. Run `npm test` to run the automated tests
4. Run `npm start` to launch `index.js` for any manual testing.
    * Add [`"type": "module"`](https://stackoverflow.com/questions/58384179/syntaxerror-cannot-use-import-statement-outside-a-module) to `package.json` to run with ES6.